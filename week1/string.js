//string length

let message = "Hello, world!";
let messageLength = message.length;
console.log(messageLength); 
 
//charAt()
let text = "Hello";

let character = text.charAt(0); 
console.log(character);

//concat()

let firstName = "John";
let lastName = "Doe";

let fullName = firstName.concat(" ", lastName);
console.log(fullName);

//index() and lastindex()

let sentence = "The quick brown fox jumps over the lazy dog";

let indexOfFox = sentence.indexOf("fox");
console.log(indexOfFox);

let lastIndexOfThe = sentence.lastIndexOf("the");
console.log(lastIndexOfThe);

//toupper and tolower
let text2 = "Hello, world!";

let upperCaseText = text2.toUpperCase(); 
console.log(upperCaseText);

let lowerCaseText = text2.toLowerCase(); 
console.log(lowerCaseText);
 
//trim()

let text3 = "   Hello, world!   ";

let trimmedText = text3.trim();
console.log(trimmedText);

//replace

let sentence2 = "The quick brown fox jumps over the lazy dog.";

let newSentence = sentence2.replace("fox", "cat");
console.log(newSentence);

//substring

let text4 = "Hello, world!";

let result1 = text4.substring(0, 5);
console.log(result1);

let result2 = text4.substring(7); 
console.log(result2);

let result3 = text4.substring(-3);
console.log(result3);

let result4 = text4.substring(5, 0); 
console.log(result4);
