//push and pop methods
let Numbers = ['one', 'second', 'third'];
Numbers.push('four'); 
console.log(Numbers); 

let removedNumbers= Numbers.pop(); 
console.log(removedNumbers);
console.log(Numbers); 
//shift and unshift methods
let numbers = [2, 3, 4];
numbers.unshift(1); 
console.log(numbers); 

let removedNumber = numbers.shift(); 
console.log(removedNumber); 
console.log(numbers);
//slice method
let animals = ['lion', 'tiger', 'elephant', 'giraffe', 'zebra'];

let slicedAnimals = animals.slice(1, 4); 
console.log(slicedAnimals);
