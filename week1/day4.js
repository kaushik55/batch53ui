//program to find wheather a substring is present in the given sentence
function isSubstringPresent(sentence, substring) {
    return sentence.includes(substring);
  }
  
  const inputSentence = "Hello world";
  const inputSubstring = "world";
  const output = isSubstringPresent(inputSentence, inputSubstring);
  console.log(output);
  //function that filters the string from an array,keeping those strings whose length is greater than the specified length.
  function filterStringsByLength(arr, length) {
    return arr.filter(str => str.length > length);
  }
  
  
  const inputArray = ["java", "Python", "HTMl", "CSS"];
  const specifiedLength = 4;
  const output1 = filterStringsByLength(inputArray, specifiedLength);
  console.log(output1);