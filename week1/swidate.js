const currenDate = new Date();
const day = currenDate.getDate();
const month = currenDate.getMonth() + 1;
const year = currenDate.getFullYear();
function printDate() {
    console.log("currenDate:");
    switch (month) {
        case 1:
          console.log(`January ${day}, ${year}`);
          break;
        case 2:
          console.log(`February ${day}, ${year}`);
          break;
        case 3:
          console.log(`March ${day}, ${year}`);
          break;
        case 4:
          console.log(`April ${day}, ${year}`);
          break;
        case 5:
          console.log(`May ${day}, ${year}`);
          break;
        case 6:
          console.log(`June ${day}, ${year}`);
          break;
        case 7:
          console.log(`July ${day}, ${year}`);
          break;
        case 8:
          console.log(`August ${day}, ${year}`);
          break;
        case 9:
          console.log(`September ${day}, ${year}`);
          break;
        case 10:
          console.log(`October ${day}, ${year}`);
          break;
        case 11:
          console.log(`November ${day}, ${year}`);
          break;
        case 12:
          console.log(`December ${day}, ${year}`);
          break;
        default:
          console.log("Invalid month");
      }
    }
printDate();

